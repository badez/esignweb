import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatCardModule } from "@angular/material/card";
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { Angular2FontawesomeModule } from "angular2-fontawesome/angular2-fontawesome";

import { AppComponent } from "./app.component";
import { SignaturePadModule } from "angular2-signaturepad";
import { SignatureComponent } from "./pages/signature/signature.component";
import { AgreementComponent } from "./pages/agreement/agreement.component";
import { AgreementViewerComponent } from "./pages/agreement/agreement-viewer/agreement-viewer.component";
import { HeaderComponent } from './shared/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    SignatureComponent,
    AgreementComponent,
    AgreementViewerComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    AngularFontAwesomeModule,
    Angular2FontawesomeModule,
    SignaturePadModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
