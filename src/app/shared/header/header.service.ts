import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  headerName: string = '';

  constructor() { }

  setHeaderName(route: string): void {
    this.headerName = route.replace('/', '');
  }
}
