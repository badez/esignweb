import { Component, ViewChildren, QueryList, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SignatureComponent } from "../../signature/signature.component";

@Component({
  selector: "app-agreement-viewer",
  templateUrl: "./agreement-viewer.component.html",
  styleUrls: ["./agreement-viewer.component.scss"]
})
export class AgreementViewerComponent {
  public title = "";
  public form: FormGroup;

  @ViewChildren(SignatureComponent) public sigs: QueryList<SignatureComponent>;
  @ViewChildren("sigContainer1") public sigContainer1: QueryList<ElementRef>;

  constructor(fb: FormBuilder) {
    this.form = fb.group({
      signatureField1: ["", Validators.required]
    });
  }

  public ngAfterViewInit() {
    this.beResponsive();
    this.setOptions();
  }

  // set the dimensions of the signature pad canvas
  public beResponsive() {
    console.log("Resizing signature pad canvas to suit container size");
    this.size(this.sigContainer1.first, this.sigs.first);
  }

  public size(container: ElementRef, sig: SignatureComponent) {
    sig.signaturePad.set("canvasWidth", container.nativeElement.clientWidth);
    sig.signaturePad.set("canvasHeight", container.nativeElement.clientHeight);
  }

  public setOptions() {
    this.sigs.first.signaturePad.set("penColor", "rgb(255, 0, 0)");
  }

  public submit() {
    console.log("CAPTURED SIGS:");
    console.log(this.sigs.first.signature);
  }

  public clear() {
    this.sigs.first.clear();
  }
}
