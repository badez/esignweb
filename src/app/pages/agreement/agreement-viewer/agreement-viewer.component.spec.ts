import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgreementViewerComponent } from './agreement-viewer.component';

describe('AgreementViewerComponent', () => {
  let component: AgreementViewerComponent;
  let fixture: ComponentFixture<AgreementViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgreementViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreementViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
